#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

int stackInit(intstack_t* self){
    self->memStart = malloc(20*sizeof(int));
    (self->count) = 0;
    self->initLen = 20;

    if(self->memStart == NULL){
        fprintf(stderr, "Initializierung fehlgeschlagen\n");
        return 1;
    }
    return 0;
}

void stackRelease(intstack_t* self){
    free((self->memStart));
}

void stackPush(intstack_t* self, int i){
    (self->count) = self->count + 1;
    if(self->count > self->initLen){
        int*h = self->memStart;
        self->initLen += 20;
        self->memStart = realloc((h), (self->initLen)*sizeof(int));
        if(self->memStart == NULL){
            fprintf(stderr, "Initializierung fehlgeschlagen\n");
        } 
    }

    int*h;
    h = (self->memStart) + self->count;
    *h = i;
    
}

int stackTop(const intstack_t* self){
    int* i;
    i = (self->memStart) + self->count - 1;
    if(stackIsEmpty(self)){
        fprintf(stderr, "Stack hat kein oberstes Element\n");
        exit(-1);
    }
    return *i;
}

int stackPop(intstack_t* self){
    int i;
    if(stackIsEmpty(self)){
        fprintf(stderr, "Stack ist leer\n");
        exit(-1);
    }
    i = *(self->memStart) + self->count;
    (self->count) = (self->count) - 1;
    return i;  
}

int stackIsEmpty(const intstack_t* self){
    if(self->count == 0){
        return 1;
    }
    return 0;
}

void stackPrint(const intstack_t* self){
    int* g = (self->memStart) + self->count - 1;
    for(int h = (self->count); h > 0; h--){
        fprintf(stderr, "%d\n", *g);
        g--;
    }
    fprintf(stderr, "\n");
}
