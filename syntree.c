#include <stdio.h>
#include "syntree.h"
#include <stdlib.h>

int syntreeInit(syntree_t* self){
    (self->nodeStart) = calloc(20, sizeof(node));

    if((self->nodeStart) == NULL){
        fprintf(stderr, "Initializierung fehlgeschlagen");
        return 1;
    }
    self->nodeCount = 0;
    self->initNodes = 20;
    return 0;
}

void syntreeRelease(syntree_t* self){
    free(self->nodeStart);
}


syntree_nid syntreeNodeNumber(syntree_t* self, int number){
    node* n;
    n = (self->nodeStart) + (self->nodeCount);
    n->own = self->nodeCount;
    n->value = number;
    n->Type = numberNode;
    self->nodeCount += 1;
    return n->own;
}

syntree_nid syntreeNodeTag(syntree_t* self, syntree_nid id){
    node *n, *m;
    n = (self->nodeStart) + id;
    m = (self->nodeStart) + self->nodeCount;
    m->own = self->nodeCount;
    m->Type = listNode;
    self->nodeCount += 1;
    m->child[1] = n->own;
    return m->own;
}


syntree_nid syntreeNodePair(syntree_t* self, syntree_nid id1, syntree_nid id2){
    node*n,*m,*k;
    n = (self->nodeStart) + id1;
    m = (self->nodeStart) + id2;
    k = (self->nodeStart) + self->nodeCount;
    k->own = self->nodeCount;
    self->nodeCount += 1;
    k->child[1] = n->own;
    k->child[2] = m->own;
    k->Type = listNode;
    return k->own;
}


syntree_nid syntreeNodeAppend(syntree_t* self, syntree_nid list, syntree_nid elem){
    node*n, *m;
    n = (self->nodeStart) + list;
    m = (self->nodeStart) + elem;
    while(n->child[1] != 0){
        n = (self->nodeStart) +  n->child[1];
    }
    n->child[1] = m->own;
    m->Type = listNode;
    return list;
}


syntree_nid syntreeNodePrepend(syntree_t* self, syntree_nid elem, syntree_nid list){
    node*n, *m;
    n = (self->nodeStart) + list;
    m = (self->nodeStart) + elem;
    m->child[1] = n->own;
    m->Type = listNode;

    return m->own;
}


void syntreePrint(const syntree_t* self, syntree_nid root){
    node *n;
    n = (self->nodeStart) + root;
    if(n->Type == listNode){
        fprintf(stderr, "{");
        syntreePrint(self, (n->child[1]));
        syntreePrint(self, n->child[2]);
        fprintf(stderr, "}");
    }
    else{
        fprintf(stderr, "(%d)", n->value);
    }
    return;
  
}
